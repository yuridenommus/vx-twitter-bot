{
  inputs = {
    cargo2nix.url = "github:cargo2nix/cargo2nix/unstable";
    flake-utils.follows = "cargo2nix/flake-utils";
    nixpkgs.follows = "cargo2nix/nixpkgs";
  };

  outputs = { self, flake-utils, cargo2nix, nixpkgs, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "vx-twitter-bot";
        pkgs = (import nixpkgs) {
          inherit system;

          overlays = [ cargo2nix.overlays.default ];
        };

        project = let
          rustToolchain = builtins.fromTOML (builtins.readFile ./rust-toolchain.toml);
        in pkgs.rustBuilder.makePackageSet {
          rustChannel = rustToolchain.toolchain.channel;
          rustVersion = "1.64.0";
          packageFun = import ./Cargo.nix;
          extraRustComponents = rustToolchain.toolchain.components;
        };

        project-bin = (project.workspace.${name} {}).bin;
      in {
        packages = {
          default = project-bin;
          ${name} = project-bin;
        };

        devShells.default = project.workspaceShell {
        };
      }
    );
}
