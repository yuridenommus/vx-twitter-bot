use std::env;

use futures::StreamExt;
use telegram_bot::{Api, MessageEntityKind, MessageKind, SendMessage, Update, UpdateKind};

fn custom_io_error(e: impl Into<Box<dyn std::error::Error + Send + Sync>>) -> std::io::Error {
    std::io::Error::new(std::io::ErrorKind::Other, e)
}

async fn setup_telegram() -> Result<Api, Box<dyn std::error::Error + Send + Sync>> {
    let _ = dotenv::dotenv();
    let token = env::var("TELEGRAM_BOT_TOKEN").or_else(|_| dotenv::var("TELEGRAM_BOT_TOKEN"))?;
    let api = Api::new(&token);
    Ok(api)
}

async fn process_update(api: &Api, update: Update) -> Result<(), Box<dyn std::error::Error>> {
    if let UpdateKind::Message(message) = update.kind {
        if let MessageKind::Text { data, entities } = message.kind {
            for entity in entities {
                if let MessageEntityKind::Url = entity.kind {
                    let url: String = data
                        .chars()
                        .skip(entity.offset as usize)
                        .take(entity.length as usize)
                        .collect();
                    let mut url = url::Url::parse(&url)?;
                    if url.host_str() == Some("twitter.com") {
                        let host = "vxtwitter.com";
                        url.set_host(Some(&host))?;
                        url.set_query(None);

                        let mut reply_message =
                            SendMessage::new(message.chat.clone(), url.to_string());
                        reply_message.reply_to(message.id);
                        reply_message.disable_notification();
                        let _ = api.send(reply_message).await?;
                    }
                }
            }
        }
    }
    Ok(())
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let api = setup_telegram().await.map_err(custom_io_error)?;

    let api_ref = &api;
    api.stream()
        .for_each_concurrent(8, |update| async move {
            let update = match update {
                Ok(u) => u,
                _ => return (),
            };

            let _ = process_update(api_ref, update).await;
        })
        .await;
    Ok(())
}
